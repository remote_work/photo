$(function(){
    var is_mobile = ($("#is_mobile").css("display") == "block")?true:false;
    if (!is_mobile) {
        $(".wrap").addClass("pc");
    }
    
    $('body').on('click', '.grid-action', function(e){
        var href = $(this).attr('href');
        var self = this;
        $.get(href, function(){
            var pjax_id = $(self).closest('.pjax-wraper').attr('id');
            $.pjax.reload('#' + pjax_id);
        });
        return false;
    });
    
    $('body').on('click', '.controlls.ajax', function() {
        var url = $(this).attr('rel-url');
        var id = $(this).attr('rel');
        var target = $(this).attr('data-target');
        var modal = $(target);
        var modalbody = $(modal).find('.modal-body div[data-pjax-container]');
        if (!modalbody) {
            modalbody = $(modal).find('.modal-body');
        }
        $.get(url, {id:id}, function(data) {
            modalbody.html(data);
            modal.modal('show');
        }, "html");

        return false;
    });
});

function selectSlide(element, id_category) {
    id = $(element).attr("rel");
    $(".slider_item img").each(function() {
        current_id = $(this).attr("rel");
        if (id == current_id) {
            $(this).addClass("active");
        }
        else $(this).removeClass("active");
        showControlls(id);
    });
    
}

function showControlls(id_category) {
    var text = $(".slider_item[rel="+id_category+"] span").html();
    $(".panel-body button.controlls").each(function() {
        $(this).removeClass("hidden");
        $(this).attr("rel", id_category);
        if ($(this).attr("data-toggle")) {
            $(this).attr("data-title", text);
        }
    });
    
}

function hideControlls() {
    $(".panel-body button.controlls").each(function() {
        $(this).addClass("hidden");
    });
}

function openCategory(element) {
    id = $(element).attr("rel");
    window.location = "/site/category?id="+id;
}

function deleteCategory(element) {
    id = $(element).attr("rel");
    update = $(element).attr("container-update");
    $.get("/site/delete-category", {id:id}, function(data) {
        if (data.success == 1) {
            $.pjax.reload({container:update});
        }
    }, "json");
}
