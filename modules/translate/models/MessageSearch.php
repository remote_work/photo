<?php

namespace app\modules\translate\models;

use Yii;
use yii\data\ActiveDataProvider;

use app\modules\translate\models\Language;

/**
 * This is the model class for table "translate_message".
 *
 * @property integer $id
 * @property integer $id_main
 * @property integer $id_language
 * @property string $language
 * @property string $category
 * @property integer $status
 * @property string $message
 * @property string $translation
 * @property string $date_updated
 */
class MessageSearch extends Message {
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_language', 'language', 'category', 'message', 'translation', 'date_updated', 'status'], 'safe'],
        ];
    }

    public function search($params) {
        $query = Message::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id_language' => $this->id_language]);
        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['like', 'language', $this->language]);
        $query->andFilterWhere(['like', 'category', $this->category]);
        $query->andFilterWhere(['like', 'message', $this->message]);
        $query->andFilterWhere(['like', 'translation', $this->translation]);
        $query->andFilterWhere(['like', 'date_updated', $this->date_updated]);

        return $dataProvider;
    }

}
