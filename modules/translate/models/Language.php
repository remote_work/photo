<?php

namespace app\modules\translate\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\models\ActiveRecord;
use app\components\UploadImageBehavior;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $icon
 * @property integer $is_default
 */
class Language extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'translate_languages';
    }
    
    public static function getShortName($lower = false) {
        $name = (new \ReflectionClass(self::className()))->getShortName();
        return  ($lower)?strtolower($name):$name; 
    }
    
    public function loadScenario($default = false, $id_item = false) {
        $class = $this::getShortName();
        $post = Yii::$app->request->post();
        if (!$id_item)
            $this->scenario = (isset($post[$class]['scenario']))?$post[$class]['scenario']:($default?$default:"default");
        else 
            $this->scenario = (isset($post[$class][$id_item]['scenario']))?$post[$class][$id_item]['scenario']:($default?$default:"default");
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['icon', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']/*, 'maxFiles' => 5 */],
            [['is_default'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['type'], 'string', 'max' => 2],
            [['type'], 'unique']
        ];
    }
    
    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'icon',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/language/{id}',
                'url' => '@web/upload/language/{id}',
                'thumbPath' => '@webroot/upload/language/{id}/thumb',
                'thumbUrl' => '@web/upload/language/{id}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 160, 'quality' => 102],
                    'preview' => ['width' => 25, 'height' => 16],
                ],
            ],
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if (!empty($this->icon)) {
            $host = (Yii::$app instanceof \yii\console\Application)?Yii::getAlias('@web'):Yii::$app->request->hostinfo;
            $this->icon = $host.$this->getThumbUploadUrl('icon', 'preview');
        }
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => Yii::t('translate', 'Name'),
            'type' => Yii::t('translate', 'Type'),
            'icon' => Yii::t('translate', 'Icon'),
            'is_default' => Yii::t('translate', 'Is Default'),
        ];
    }
    
    public static function iconsList() {
        $icons_list = [];
        $icons = Language::find()->select(["id", "icon", "name", "type"])->all();
        
        foreach ($icons as $icon)
            $icons_list[$icon->type] = Html::img($icon->icon, ['class' => '']). "<span> " . $icon->name. "</span>";
        
        return $icons_list;
    }


    public static function getNameOfLanguage($id = false) {
        $language = Language::findOne(['id' => $id]);
        return ($language)?$language->getAttribute('name'):false;
    }
    
    public static function getTypeOfLanguage($id = false) {
        $language = Language::findOne(['id' => $id]);
        return ($language)?$language->getAttribute('type'):false;
    }
    
    public static function getLanguageId($language = "ru") {
        return ($item = Language::findOne(["type" => $language]))?$item->id:false;
    }
    
    public function setDefault() {
        Language::updateAll(['is_default' => 0], ['!=', 'id', $this->id]);
    }
    
    public static function getList() {
        return ArrayHelper::map(Language::find()->select(['type', 'id'])->orderBy("`type` DESC")->all(), 'id', 'type');
    }
    
    public static function getIcon($id = false) {
        $language = Language::findOne(['id' => $id]);
        return ($language)?$language->getAttribute('icon'):false;
    }
    
    public static function getCurrentIcon() {
        $language = Language::findOne(['type' => Yii::$app->language]);
        return ($language)?$language->getAttribute('icon'):false;
    }
    
    
    public static function getTitleOfLanguage($id = false) {
        return Html::img(self::getIcon($id))." ".self::getNameOfLanguage($id);
    }

}
