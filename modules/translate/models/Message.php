<?php

namespace app\modules\translate\models;

use Yii;
use yii\helpers\ArrayHelper;

use app\modules\translate\models\Language;

/**
 * This is the model class for table "translate_message".
 *
 * @property integer $id
 * @property integer $id_main
 * @property integer $id_language
 * @property string $language
 * @property string $category
 * @property integer $status
 * @property string $message
 * @property string $translation
 * @property string $date_updated
 */
class Message extends \yii\db\ActiveRecord {
    
    const DEFAULT_CATEGORY = 'common';

    const STATUS_NO_ERROR = 0;
    const STATUS_ERROR_NO_TRANSLATE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'translate_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_main', 'id_language', 'status'], 'integer'],
            [['category', 'message', 'translation'], 'required'],
            [['translation'], 'string'],
            [['date_updated'], 'safe'],
            [['language'], 'string', 'max' => 3],
            [['category'], 'string', 'max' => 150],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('translate', 'Id Language'),
            'language' => Yii::t('translate', 'Language'),
            'category' => Yii::t('translate', 'Category'),
            'status' => Yii::t('translate', 'Status'),
            'message' => Yii::t('translate', 'Message'),
            'translation' => Yii::t('translate', 'Translation'),
            'date_updated' => Yii::t('translate', 'Date Updated'),
        ];
    }

    public static function getCategories() {
        return ArrayHelper::map(Message::find()->select(["id", "category"])->groupBy("category")->all(), "category", "category");
    }

    public static function missingTranslation($category, $message, $language) {
        $id_language = Language::getLanguageId(strtolower($language));
        // Смотрим есть ли в базе запись
        $_main_message = Message::find()->select("id, id_main")->where(["category" => $category, "message" => $message, "id_main" => "id"])->one();
        $_message = Message::find()->select("id, status")->where(["id_language" => $id_language, "category" => $category, "message" => $message])->one();
        if (!$_message) {
            // Добавляем запись, что есть message без перевода
            $_message = new Message();
            if ($_main_message)
                $_message->id_main = $_main_message->id;
            $_message->id_language = $id_language;
            $_message->language = $language;
            $_message->category = $category;
            $_message->message = $message;
            $_message->translation = $message;
            $_message->status = self::STATUS_ERROR_NO_TRANSLATE;
            //Yii::$app->debug->show($_message);
            if ($_message->validate()) {
                if ($_message->save()) {
                    if (!isset($_message->id_main)) {
                        $_message->id_main = $_message->id;
                        $_message->update();
                    }
                }
                else Yii::$app->debug->show($_message->getErrors());
            }
            else Yii::$app->debug->show($_message->getErrors());
        } elseif ($_message->status != self::STATUS_ERROR_NO_TRANSLATE) {
            // Обновляем запись что message без перевода
            $_message->status = self::STATUS_ERROR_NO_TRANSLATE;
            $_message->update();
        }
    }

    public static function getStatusArray() {
        return [
            self::STATUS_NO_ERROR => Yii::t('translate', 'Translated'),
            self::STATUS_ERROR_NO_TRANSLATE => Yii::t('translate', 'Not translated'),
        ];
    }

}
