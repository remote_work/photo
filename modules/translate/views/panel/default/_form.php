<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\select2\Select2;

use app\modules\translate\models\Language;


/* @var $this yii\web\View */
/* @var $model app\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translate-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $form->field($model, 'id_language')->textInput() ?>
    
    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, "id_language")->widget(Select2::classname(), [
            'options' => [
                'placeholder' => Yii::t('photo', 'Select a language ..')
            ],
            'data' => ArrayHelper::map(User::find()->select(['name', 'id'])->orderBy("`name` ASC")->all(), 'id', 'name')
        ])->label(Yii::t('photo', 'User'));     
    }
    ?>

    <?= $form->field($model, 'language')->textInput(['maxlength' => true, "disabled" => "disabled"]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true, "disabled" => "disabled"]) ?>

    <?php $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true, "disabled" => "disabled"]) ?>

    <?= $form->field($model, 'translation')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'date_updated')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
