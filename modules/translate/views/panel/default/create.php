<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Message */

$this->title = Yii::t('translate', 'Create Translate Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('translate', 'Translate Messages'), 'url' => ['/translate/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translate-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
