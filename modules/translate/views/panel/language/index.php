<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('translate', 'Languages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('translate', 'Translate Messages'), 'url' => ['/translate/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('translate', 'Add Language'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function($model) {
                    return ($model->icon)?Html::img($model->icon):false;
                }
            ],
            'name',
            'type',
            'is_default',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
