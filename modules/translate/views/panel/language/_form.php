<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Language */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="language-form">

    <?php 
    $form = ActiveForm::begin([
        //'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'scenario')->hiddenInput()->label(false);
    echo $form->field($model, 'icon')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            //'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('translate', 'Select icon..'),
            'initialPreview'=> ($model->icon)?Html::img($model->getThumbUploadUrl('icon', 'thumb'), ['class' => 'img-thumbnail']):false, 
            'overwriteInitial'=>true,
        ],
        'pluginEvents' => [
            "fileclear" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('default');}",
            "fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('update')}",
        ],
    ]);
    ?>

    <?= $form->field($model, 'is_default')->dropDownList(["0" => Yii::t('common', "yes"), "1" => Yii::t('common', "no")]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
