<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Language */

$this->title = Yii::t('translate', 'Update Language: ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('translate', 'Translate Messages'), 'url' => ['/translate/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('translate', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="language-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
