<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Language */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('translate', 'Translate Messages'), 'url' => ['/translate/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('translate', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            
            [
                'label' => Yii::t('translate', 'Icon'),
                'format' => 'raw',
                'value' => Html::img($model->getThumbUploadUrl('icon', 'preview'), ['class' => 'img-thumbnail']),
            ],
            'name',
            'type',
            'is_default',
        ],
    ]) ?>

</div>
