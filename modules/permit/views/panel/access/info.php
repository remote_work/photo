<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Links */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Результат генерации ролей для пользователей';
$this->params['breadcrumbs'][] = ['label' => 'Управление доступом', 'url' => ['/panel/permit/default/index']];
$this->params['breadcrumbs'][] = 'Результат выполнения';
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $message ?>

</div>
