<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\permit\models\AuthAssignment;

$this->title = 'Управление доступом';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список ролей', ['/panel/permit/access/role'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Список правил доступа', ['/panel/permit/access/permission'], ['class' => 'btn btn-success']) ?>
        <?php
        if (AuthAssignment::needGenerateAssigment())
            echo Html::a('Генерировать роли', ['/panel/permit/access/generate'], ['class' => 'btn btn-primary', 'title' => "Данные беруться из таблицы пользователей"]); 
        ?>
    </p>
<?php
$dataProvider = new ArrayDataProvider([
      'allModels' => Yii::$app->authManager->getRoles(),
      'sort' => [
          'attributes' => ['name', 'description'],
      ],
      'pagination' => [
          'pageSize' => 10,
      ],
 ]);
?>

<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class'     => DataColumn::className(),
            'attribute' => 'name',
            'label'     => 'Роль'
        ],
        [
            'class'     => DataColumn::className(),
            'attribute' => 'description',
            'label'     => 'Описание'
        ],
        [
            'label'     => 'Количество пользователей',
            'format'    => ['html'],
            'value'     => function($data) { 
                return AuthAssignment::getCountByType($data->name);
            }
        ],
    ]
]);
?>
</div>
