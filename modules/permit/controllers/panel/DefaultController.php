<?php

namespace app\modules\permit\controllers\panel;

use Yii;
use yii\web\Controller;

class DefaultController extends Controller {
    
    
    public function showError($exception) {
        $name = $exception->getName() . " (#". $exception->statusCode . ")";
        return $this->render('error', ['exception' => $exception, 'name' => $name, "message" => $exception->getMessage()]);
    }

    public function actionIndex() {
        return $this->render('index');
    }

}