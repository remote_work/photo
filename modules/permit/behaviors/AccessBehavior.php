<?php

namespace app\modules\permit\behaviors;

use Yii;
use yii\helpers\Url;
use yii\behaviors\AttributeBehavior;
use yii\di\Instance;
use yii\base\Module;
use yii\web\Application;
use yii\web\User;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class AccessBehavior extends AttributeBehavior {

    public $rules = [];
    private $_rules = [];

    public function events() {
        return [
            Module::EVENT_BEFORE_ACTION => 'interception',
        ];
    }

    public function interception($event) {
        $route[0] = Yii::$app->getRequest()->getPathInfo();
        $route[1] = Yii::$app->getRequest()->getQueryParams();
        
        //Yii::$app->debug->show($route);
        
        if (\Yii::$app->getUser()->isGuest &&
            \Yii::$app->getRequest()->url !== Url::to(\Yii::$app->getUser()->loginUrl)
        ) {
            if (!in_array("/".$route[0], [Url::toRoute("signup"), Url::toRoute("error"), Url::toRoute("contact"), Url::toRoute("captcha")])) {
                \Yii::$app->getResponse()->redirect(\Yii::$app->getUser()->loginUrl);
            }
        }
        
        
        //$route = Yii::$app->getRequest()->resolve();
        
        //Yii::$app->debug->show($route);

        //Проверяем права по конфигу
        $this->createRule();
        $user = Instance::ensure(Yii::$app->user, User::className());
        $request = Yii::$app->getRequest();
        
        //Создаем правило, если его нет в базе доступа, но есть роль в классе пользователя
        $module = Yii::$app->getModule('permit');
        if (!Yii::$app->user->isGuest && $module->rolesFromUser) { //Доступна только одна роль
            $fieldOfRole = $module->fieldOfRole;
            $userRole = $user->identity->$fieldOfRole;
            $rolesList = Yii::$app->authManager->getAssignments($user->id);
            if ($role = Yii::$app->authManager->getRole($userRole)) {
                if (!$rolesList) 
                    Yii::$app->authManager->assign($role, $user->id);
                else if (!isset($rolesList[$userRole])) {
                    Yii::$app->authManager->revokeAll($user->id);
                    Yii::$app->authManager->assign($role, $user->id);
                }
            }
        }
        //В зависмости, откуда вызывается событие, будет разная структура sender
        $action = isset($event->sender->requestedAction)?$event->sender->requestedAction:$event->sender->module->requestedAction;

        //Yii::$app->debug->show($route);
        if (!$this->cheсkByRule($action, $user, $request)) {
            //И по AuthManager
            if (!$this->checkPermission($route))  
                $this->showError(new ForbiddenHttpException('Не достаточно прав'), $route);
        }
    }
    
    protected function showError($exception, $route) {
        $p = Yii::$app->createController('permit/panel/default/error');
        if (Yii::$app->request->isAjax) {
            $path = @$route[0];
            echo json_encode([
                "error" => "Не достаточно прав. Пожалуйста убедитесь, что у вас есть доступ к '{$path}'",
            ]);
        }
        else echo $p[0]->showError($exception);
        Yii::$app->end();
    }

    protected function createRule() {
        foreach ($this->rules as $controller => $rule) {
            foreach ($rule as $singleRule) {
                if (is_array($singleRule)) {
                    $option = [
                        'controllers' => [$controller],
                        'class' => 'yii\filters\AccessRule'
                    ];
                    $this->_rules[] = Yii::createObject(array_merge($option, $singleRule));
                }
            }
        }
    }

    protected function cheсkByRule($action, $user, $request) {
        foreach ($this->_rules as $rule) {
            if ($rule->allows($action, $user, $request))
                return true;
        }
        return false;
    }

    protected function checkPermission($route) {
        //$route[0] - is the route, $route[1] - is the associated parameters

        $routePathTmp = explode('/', $route[0]);
        $routeVariant = array_shift($routePathTmp);
        if (Yii::$app->user->can($routeVariant, $route[1]))
            return true;

        foreach ($routePathTmp as $routePart) {
            $routeVariant .= '/' . $routePart;
            if (Yii::$app->user->can($routeVariant, $route[1]))
                return true;
        }

        return false;
    }

}
