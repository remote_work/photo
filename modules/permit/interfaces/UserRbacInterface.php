<?php
namespace app\modules\permit\interfaces;


interface UserRbacInterface {

    public function getId();
    public function getUserName();
    public static function findIdentity($id);
}