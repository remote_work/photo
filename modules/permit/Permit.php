<?php

namespace app\modules\permit;

use Yii;
use app\modules\permit\components\ErrorHandler;

class Permit extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\permit\controllers';
    public $userClass = false;
    public $rolesFromUser = false;
    public $fieldOfRole = "role";

    public function init() {
        parent::init(); 
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['db_rbac'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-Ru',
            'basePath' => 'app\modules\permit\messages',
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/permit/' . $category, $message, $params, $language);
    }
}
