<?php
namespace app\modules\permit\migrations;

use yii\db\Schema;
use yii\db\Migration;

class permit_init extends Migration {

    public function up() {
        $sql = file_get_contents(dirname(__FILE__) . "/data/init.sql");
        $this->execute($sql);
    }

    public function down() {
        echo "m150722_231952_init cannot be reverted.\n";

        return false;
    }

}
