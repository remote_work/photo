<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\PhotoComments */

$this->title = Yii::t('photo', 'Add Comments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('photo', 'Photo Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-comments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
