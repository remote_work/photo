<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\select2\Select2;

use app\models\User;
use app\modules\photo\models\Photo;
use app\modules\photo\models\PhotoComments;

/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\PhotoComments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-comments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, "id_user")->widget(Select2::classname(), [
        'options' => [
            'placeholder' => Yii::t('photo', 'Select a user ..')
        ],
        'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
    ])->label(Yii::t('photo', 'User'));     
    ?>
    
    <?php
    echo $form->field($model, "id_photo")->widget(Select2::classname(), [
        'options' => [
            'placeholder' => Yii::t('photo', 'Select a photo ..')
        ],
        'data' => ArrayHelper::map(Photo::find()->select(["CONCAT(title,' (',id,')') as `title`", 'id'])->orderBy("`title` ASC")->all(), 'id', 'title')
    ])->label(Yii::t('photo', 'Photo'));     
    ?>

    <?php
    echo $form->field($model, "id_comment")->widget(Select2::classname(), [
        'options' => [
            'placeholder' => Yii::t('photo', 'Select a parent comment ..')
        ],
        'data' => ArrayHelper::map(PhotoComments::find()->select(["SUBSTRING(`message`, 1, 30) as message", 'id'])->orderBy("`message` ASC")->all(), 'id', 'message')
    ])->label(Yii::t('photo', 'Parent Comment'));     
    ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'likes')->textInput() ?>

    <?= $form->field($model, 'dislikes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
