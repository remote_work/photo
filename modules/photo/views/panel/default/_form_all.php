<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-images-form">

    <?php 
    $form = ActiveForm::begin([
        //'enableClientValidation' => false,
        'action'  => 'update-all',
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>
    
    <?php foreach ($models as $model) { ?>
        <div id='block-<?= $model->id ?>' class="col-md-12">
            <?= $form->field($model, "[{$model->id}]title")->textInput(); ?>
        </div>
        <div class="col-md-3" style="padding-left: 0px;">
            <?= Html::img($model->getThumbUploadUrl('file', 'thumb'), ['class' => 'img-thumbnail', 'style' => "width: 100%;"]); ?>
        </div>
        <div class="col-md-9" style="padding: 0px;">
            <?= $form->field($model, "[{$model->id}]description")->textarea(['rows' => 7]); ?>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("photo", 'Update All'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
