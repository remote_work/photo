<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\Photo */

$this->title = Yii::t('photo', 'Update Photo: ') . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('photo', 'Photos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="photo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
