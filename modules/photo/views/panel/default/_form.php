<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\file\FileInput;
use kartik\select2\Select2;

use app\models\User;
use app\modules\photo\models\PhotoCategories;

/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\Photo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-form">

    <?php 
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>
   
    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, "id_user")->widget(Select2::classname(), [
            'options' => [
                'placeholder' => Yii::t('photo', 'Select a user ..')
            ],
            'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
        ])->label(Yii::t('photo', 'User'));     
    }
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'scenario')->hiddenInput()->label(false);
    echo $form->field($model, '_files[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('photo', 'Select photo..'),
            'initialPreview'=> ($model->file)?Html::img($model->getThumbUploadUrl('file', 'thumb'), ['class' => 'img-thumbnail']):false, 
            'overwriteInitial'=>true,
        ],
        'pluginEvents' => [
            "fileclear" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('default');}",
            "fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('update')}",
        ],
    ]);
    ?>
    
    <?php $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    
    <?php
        echo $form->field($model, "id_category")->widget(Select2::classname(), [
            'options' => [
                'placeholder' => Yii::t('photo', 'Without category')
            ],
            'data' => ArrayHelper::map(PhotoCategories::find()->select(["title", 'id'])->orderBy("`title` ASC")->all(), 'id', 'title')
        ])->label(Yii::t('photo', 'Category'));
    ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'size')->textInput() ?>

    <?php $form->field($model, 'ext')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'position')->textInput() ?>

    <?php $form->field($model, 'date_create')->textInput() ?>

    <?php $form->field($model, 'date_update')->textInput() ?>

    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, 'is_active')->dropDownList(["0" => Yii::t('common', "yes"), "1" => Yii::t('common', "no")]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
