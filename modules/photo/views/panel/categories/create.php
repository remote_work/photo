<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\PhotoCategories */

$this->title = Yii::t('photo', 'Add Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('photo', 'Photo Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
