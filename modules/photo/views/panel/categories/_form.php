<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\PhotoCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, "id_user")->widget(Select2::classname(), [
            'options' => [
                'placeholder' => Yii::t('photo', 'Select a user ..')
            ],
            'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
        ])->label(Yii::t('photo', 'User'));     
    }
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_show')->dropDownList(["0" => Yii::t('common', "no"), "1" => Yii::t('common', "yes")]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
