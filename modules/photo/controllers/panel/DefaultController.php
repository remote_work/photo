<?php

namespace app\modules\photo\controllers\panel;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use app\modules\photo\models\Photo;

/**
 * DefaultController implements the CRUD actions for Photo model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Photo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Photo::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Photo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Photo();
        $model->scenario = "insert";
        $model->id_user = Yii::$app->user->id;
        $model->date_create = date("Y-m-d G:i:s", time());
        $model->is_active = 1;
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->_files = UploadedFile::getInstances($model, '_files')) {
                $model->file = array_shift($model->_files);
            }
                
            if ($model->save()) {
                $photos = [$model];
                if (!empty($model->_files)) {
                    foreach ($model->_files as $file) {
                        if ($file instanceof UploadedFile) {
                            $photo = new Photo(); 
                            $photo->setAttributes($model->attributes);
                            $photo->scenario = "insert";
                            $photo->file = $file;
                            $photo->title = "";
                            //Yii::$app->debug->show($photo);
                            if ($photo->validate()) {
                                if ($photo->save()) {
                                    $photos[] = $photo;
                                }
                            }
                        }
                    }
                }
                if (count($photos) > 1) {
                    return $this->render('update_all', ['models' => $photos]);
                }
                else return $this->redirect(['view', 'id' => $model->id]);
            }
                
        } 
            
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Photo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->loadScenario("update");

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateAll() {
        
        $models = [];
        $postData = Yii::$app->request->post("Photo");
        if ($postData) {
            foreach ($postData as $id => $single) {
                $models[$id] = Photo::findOne(["id" => $id]);
            }
        }
        
        //Yii::$app->debug->show($models);
        if (Photo::loadMultiple($models, Yii::$app->request->post()) && Photo::validateMultiple($models)) {
            foreach ($models as $model) {
                $model->save();
            }
            return $this->redirect(['index']);
        }
        
        return $this->render('update_all', [
            'models' => $models,
        ]);
        
    }

    /**
     * Deletes an existing Photo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
