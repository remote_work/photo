<?php

namespace app\modules\photo\models;

use Yii;

/**
 * This is the model class for table "photo_comments".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_photo
 * @property integer $id_comment
 * @property string $message
 * @property integer $likes
 * @property integer $dislikes
 * @property string $date_created
 * @property string $date_updated
 */
class PhotoComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_photo',], 'required'],
            [['id_user', 'id_photo', 'id_comment', 'likes', 'dislikes'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['message'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('photo', 'ID'),
            'id_user'           => Yii::t('photo', 'Id User'),
            'id_photo'          => Yii::t('photo', 'Id Photo'),
            'id_comment'        => Yii::t('photo', 'Id Comment'),
            'message'           => Yii::t('photo', 'Message'),
            'likes'             => Yii::t('photo', 'Likes'),
            'dislikes'          => Yii::t('photo', 'Dislikes'),
            'date_created'      => Yii::t('photo', 'Date Created'),
            'date_updated'      => Yii::t('photo', 'Date Updated'),
        ];
    }
}
