<?php

namespace app\modules\photo\models;

use Yii;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii2mod\bxslider\BxSlider;

use app\modules\photo\models\Photo;

/**
 * This is the model class for table "photo_categories".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $slug
 * @property string $title
 * @property integer $position
 * @property integer $is_show
 * @property string $date_created
 * @property string $date_updated
 */
class PhotoCategories extends \yii\db\ActiveRecord {
    
    public $icon = '@webroot/upload/default.jpg';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user',], 'required'],
            [['id_user', 'position', 'is_show'], 'integer'],
            [['title'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['slug', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'id_user'           => Yii::t('photo', 'Id User'),
            'slug'              => Yii::t('photo', 'Path'),
            'title'             => Yii::t('photo', 'Title'),
            'position'          => Yii::t('photo', 'Position'),
            'is_show'           => Yii::t('photo', 'Is Show'),
            'date_created'      => Yii::t('photo', 'Date Created'),
            'date_updated'      => Yii::t('photo', 'Date Updated'),
        ];
    }
    
    function behaviors() {
        return [
            'slug' => [
                'class' => 'app\modules\photo\components\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ]
        ];
    }
    
    public static function getDefaultIcon() {
        return Yii::getAlias("@web")."/upload/default.jpg";
    }
    
    public function getPhoto() {
        return $this->hasOne(Photo::className(), ['id_category' => 'id']);
    }
    
    public static function getAll() {
        $categories = false;
        if ($categories = PhotoCategories::find()->where(["is_show" => 1])->orderBy("position ASC")->all()) {
            foreach ($categories as &$category) {
                if ($photo = $category->photo) {
                    $category->icon = $photo->getThumbUploadUrl('file', 'preview');
                }
                else $category->icon = self::getDefaultIcon ();
            }
        }
        return $categories;
    }
    
    public static function buildSlider($max_slides = 7) {
        $gallery = false;
        $images = self::getAll();
        if ($images) {
            $images_items = [];
            foreach ($images as $image)
                $images_items[] = "<div class=\"slider_item\" rel=\"{$image->id}\">".Html::a(Html::img($image->icon, [
                    'class' => !empty($image->is_active)?'img-thumbnail active':'img-thumbnail',
                    'onclick' => "selectSlide(this, {$image->id})",
                    'rel' => $image->id
                ]), null /*Url::to(["category", "id" => $image->id])*/).
                Html::tag("span", $image->title, [
                    //'class' => "glyphicon glyphicon-trash", 
                ])."</div>";
                    
            $gallery = BxSlider::widget([
                'containerOptions' => [
                    //'style' => "margin-bottom: 5px;"
                ],
                'pluginOptions' => [
                    'minSlides' => 1,
                    'maxSlides' => (count($images_items) > $max_slides)?$max_slides:false,
                    'controls' => (count($images_items) > $max_slides)?true:false,
                    // set video property to true, if in $items array exist videos
                    'video' => false,
                    'slideWidth' => 150,
                    'pager' => false,
                    // usage events
                    'onSliderLoad' => new \yii\web\JsExpression('
                        function() {
                            //alert("Slider load");
                        },
                    ')
                ],
                'items' => $images_items 
            ]); 
        }
        return $gallery;
    }
}
