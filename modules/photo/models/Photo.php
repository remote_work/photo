<?php

namespace app\modules\photo\models;

use Yii;

use app\models\ActiveRecord;
use app\components\UploadImageBehavior;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_category
 * @property string $slug
 * @property string $title
 * @property string $file
 * @property string $url
 * @property string $description
 * @property integer $size
 * @property string $ext
 * @property integer $position
 * @property string $date_create
 * @property string $date_update
 * @property integer $is_active
 * @property integer $is_delete
 */
class Photo extends ActiveRecord {
    
    public $_files;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'id_category', 'size', 'position', 'is_active', 'is_delete'], 'integer'],
            [['id_user', '_files'], 'required'],
            [['date_create', 'date_update'], 'safe'],
            [['slug', 'title', 'url'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['ext'], 'string', 'max' => 10],
            ['_files', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update'], 'maxFiles' => 10],
            //['_files', 'emptyFiles'],
            ['file', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']/*, 'maxFiles' => 5 */],
        ];
    }
    
    public function emptyFiles($attribute, $params) {
        Yii::$app->debug->show($attribute);
        if (empty($this->$attribute)) {
            $this->addError("_files", Yii::t("photo", "Please chose photo"));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                => 'ID',
            'id_user'           => Yii::t('photo', 'Id User'),
            'id_category'       => Yii::t('photo', 'Id Category'),
            'slug'              => Yii::t('photo', 'Path'),
            'title'             => Yii::t('photo', 'Title'),
            'file'              => Yii::t('photo', 'File'),
            '_files'            => Yii::t('photo', 'Photo'),
            'url'               => Yii::t('photo', 'Url'),
            'description'       => Yii::t('photo', 'Description'),
            'size'              => Yii::t('photo', 'Size'),
            'ext'               => Yii::t('photo', 'Ext'),
            'position'          => Yii::t('photo', 'Position'),
            'date_create'       => Yii::t('photo', 'Date Create'),
            'date_update'       => Yii::t('photo', 'Date Update'),
            'is_active'         => Yii::t('photo', 'Is Active'),
            'is_delete'         => Yii::t('photo', 'Is Delete'),
        ];
    }

    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'file',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/photos/{id}',
                'url' => '@web/upload/photos/{id}',
                'thumbPath' => '@webroot/upload/photos/{id}/thumb',
                'thumbUrl' => '@web/upload/photos/{id}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                ],
            ],
            'slug' => [
                'class' => 'app\modules\photo\components\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ]
        ];
    }
    
    public static function getAll($id_category = false) {
        if ($id_category) {
            return Photo::find()->where(["is_active" => 1, "id_category" => $id_category])->all();
        }
        else return Photo::find()->where(["is_active" => 1])->all();
    }

}
