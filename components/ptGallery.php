<?php
namespace app\components;

use Yii;
use yii\bootstrap\Html;

use dosamigos\gallery\Gallery;

class ptGallery extends \dosamigos\gallery\Gallery {
    
    public function renderTemplate() {
        $template[] = '<div class="slides"></div>';
        $template[] = '<h3 class="title"></h3>';
        $template[] = '<p class="description"></p>';
        $template[] = '<p class="messages-box">1</p>';
        $template[] = '<a class="prev">‹</a>';
        $template[] = '<a class="next">›</a>';
        $template[] = '<a class="close">×</a>';
        $template[] = '<a class="play-pause"></a>';
        $template[] = '<ol class="indicator"></ol>';

        return Html::tag('div', implode("\n", $template), $this->templateOptions);
    }
    
        
}

