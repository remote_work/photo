<?php

namespace app\components;

use Yii;

class Debug extends yii\base\Component {
    
    public function show($data = []) {
        header ("Content-Type: text\plain");
        print_r($data); die();
    }
    
    public function showAll($data = []) {
        header ("Content-Type: text\plain");
        var_dump($data); die();
    }
}

