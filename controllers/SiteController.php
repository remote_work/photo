<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

use app\modules\photo\models\Photo;
use app\modules\photo\models\PhotoCategories;

class SiteController extends Controller {
    
    public function init() {
        parent::init();
        $this->layout = "site";
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $items = array();
        if ($photos = Photo::getAll()) {
            foreach ($photos as $photo) {
                $items[] = [
                    'url' => $photo->getUploadUrl('file'),
                    'src' => $photo->getThumbUploadUrl('file', 'preview'),
                    'options' => array('title' => $photo->title, "data-description" => "This is description"),
                ];
            }
        }
        
        $categories = array();
        if ($categories_list = PhotoCategories::getAll()) {
            foreach ($categories_list as $category) {
                $categories[] = [
                    'title' => $category->title,
                    'url' => Url::to(["category", "id" => $category->id]),
                    'src' => $category->icon
                ];
            }
        }
//        $categories[] = [
//            'title' => $category->title,
//            'url' => Url::to(["add-category", "id" => $category->id]),
//            'src' => $category->icon
//        ];
        return $this->render('index', ["items" => $items, "categories" => $categories]);
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

//    public function actionAbout() {
//        return $this->render('about');
//    }
    
    public function actionCategory($id = false) {
        $model = $this->findCategory($id);
        
        $items = array();
        if ($photos = Photo::getAll($id)) {
            foreach ($photos as $photo) {
                $items[] = [
                    'url' => $photo->getUploadUrl('file'),
                    'src' => $photo->getThumbUploadUrl('file', 'preview'),
                    'options' => array('title' => $photo->title),
                ];
            }
        }
        
        return $this->render('category', ["items" => $items, "model" => $model]);
    }
       
    protected function findCategory($id) {
        if (($model = PhotoCategories::find()->where(["id" => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function confirmAuth() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect("login");
        }
    }
    
    public function actionAddCategory() {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        $model = new PhotoCategories();
        $model->id_user = Yii::$app->user->id;
        $model->is_show = 1;
        $model->date_created = date("Y-m-d G:i:s", time());

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        } 
        return true;
    }
    
    public function actionEditCategory($id = false) {
        $this->confirmAuth();
        
        if (!empty($id)) {
            $model = $this->findCategory($id);
        } else $model = new PhotoCategories;
        
        $model->id_user = Yii::$app->user->id;
        $model->is_show = 1;
        $model->date_created = date("Y-m-d G:i:s", time());

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return true;
            } else return json_encode ($model->getErrors ());
        }
        
        return $this->renderAjax('forms/_edit_category', [
            'model' => $model,
        ]);
    }
    
    public function actionDeleteCategory($id = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($this->findCategory($id)->delete()) {
            return ["success" => 1];
        }
        else return ["success" => 0];
    }
    
    public function actionUpload($id = false) {
        if (!($category = $this->findCategory($id))) {
            $this->redirect("index");
        }
        $model = new Photo();
        $model->scenario = "insert";
        $model->id_user = Yii::$app->user->id;
        $model->date_create = date("Y-m-d G:i:s", time());
        $model->id_category = $category->id;
        $model->is_active = 1;
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->_files = UploadedFile::getInstances($model, '_files')) {
                $model->file = array_shift($model->_files);
            }
                
            if ($model->save()) {
                $photos = [$model];
                if (!empty($model->_files)) {
                    foreach ($model->_files as $file) {
                        if ($file instanceof UploadedFile) {
                            $photo = new Photo(); 
                            $photo->setAttributes($model->attributes);
                            $photo->scenario = "insert";
                            $photo->file = $file;
                            $photo->title = "";
                            //Yii::$app->debug->show($photo);
                            if ($photo->validate()) {
                                if ($photo->save()) {
                                    $photos[] = $photo;
                                }
                            }
                        }
                    }
                }
                if (count($photos) > 1) {
                    return $this->render('update_all', ['models' => $photos, "category" => $id]);
                }
                else return $this->redirect(['category', 'id' => $id]);
            }
            //else Yii::$app->debug->show($model);
                
        } 
            
        return $this->render('upload', [
            'model' => $model,
            "category" => $category,
        ]);
    }
    
    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect("index");
                }
            }
        }
        
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

}
