<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

use kartik\sidenav\SideNav;
use app\assets\AppAsset;
use app\assets\PanelAsset;

AppAsset::register($this);
PanelAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('common', 'Dinamic Photos'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            //['label' => 'Users', 'url' => ['/user/index']],
            //['label' => 'About', 'url' => ['/site/about']],
            //['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-md-2">
                <?php
                // ['label' => '<span class="pull-right badge">10</span> New Arrivals', 'url' => Url::to(['/site/new-arrivals', 'type'=>$type]), 'active' => ($item == 'new-arrivals')],
                
                if (Yii::$app->user->can("panel/permit")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Permissions"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Permission Info'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/permit/default/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Roles List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/permit/access/role']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add Role'), 'icon' => 'plus', 'url' => Url::to(['/permit/panel/access/add-role']), 'active' => false],
                            ['label' => Yii::t("menu", 'Permission List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/permit/access/permission']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add Permission'), 'icon' => 'plus', 'url' => Url::to(['/permit/panel/access/add-permission']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("panel/photo")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Photos"),
                        'items' => [ //   
                            ['label' => Yii::t("menu", 'Photos List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/photo/default/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Upload Photo'), 'icon' => 'plus', 'url' => Url::to(['/panel/photo/default/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Categories List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/photo/categories/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add Category'), 'icon' => 'plus', 'url' => Url::to(['/panel/photo/categories/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/photo/comments/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add Comment'), 'icon' => 'plus', 'url' => Url::to(['/panel/photo/comments/create']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("panel/user")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Users"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Users List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/user/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add User'), 'icon' => 'plus', 'url' => Url::to(['/panel/user/create']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("panel/translate")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Localisation"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Translates'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/translate/default/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Languages List'), 'icon' => 'list-alt', 'url' => Url::to(['/panel/translate/language/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add Language'), 'icon' => 'plus', 'url' => Url::to(['/panel/translate/language/create']), 'active' => false],
                        ],
                    ]); 
                }
                ?>
            </div>
            <div class="col-md-10">
            <?php } else {
                echo '<div></div>';
            }
            ?>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::t('common', 'Dinamic Photos') ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
