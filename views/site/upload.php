<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\file\FileInput;
use kartik\select2\Select2;

use app\models\User;
use app\modules\photo\models\PhotoCategories;


/* @var $this yii\web\View */
/* @var $model app\modules\photo\models\Photo */

$this->title = Yii::t('client', 'Upload Photo');
$this->params['breadcrumbs'][] = ['label' => $category->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="photo-form">

    <?php 
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'scenario')->hiddenInput()->label(false);
    echo $form->field($model, '_files[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('client', 'Select photos..'),
            'initialPreview'=> ($model->file)?Html::img($model->getThumbUploadUrl('file', 'thumb'), ['class' => 'img-thumbnail']):false, 
            'overwriteInitial'=>true,
        ],
        'pluginEvents' => [
            "fileclear" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('default');}",
            "fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('update')}",
        ],
    ]);
    ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
