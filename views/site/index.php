<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use app\components\ptGallery;
use \dosamigos\gallery\Carousel;

use yii\bootstrap\Modal;

use app\modules\photo\models\PhotoCategories;

/* @var $this yii\web\View */

$this->title = Yii::t('client', 'Dinamic Photos');


?>
<div class="site-index">
    
    <div classs="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3><?= Yii::t('client', 'Categories'); ?></h3>
            </div>
            <div class="panel-body">
                <div class="btn-group" role="group">
                    <?= Html::button(Html::tag("span", null, ["class" => "glyphicon glyphicon-plus", "aria-hidden" => true])." ".Yii::t('client', "Add"), ["class" => "btn btn-success", 'data' => ['toggle' => 'modal', 'target' => "#edit-category",]]) ?>    
                </div>
                <div class="btn-group" role="group">
                    <?= Html::button(Html::tag("span", null, ["class" => "glyphicon glyphicon-search", "aria-hidden" => true])." ".Yii::t('client', "Open"), ["class" => "btn btn-info controlls hidden", "onclick" => "openCategory(this)"]) ?>    
                </div>
                <div class="btn-group" role="group">
                    <?= 
                    $this->render("_modal_button", ["button" => [
                        "name" => Yii::t("client", "Rename"), 
                        "url" => Url::to("/site/edit-category"), 
                        "class" => "btn btn-info controlls ajax hidden", 
                        "reload" => "#_view_categories"]]
                    );
                    ?>
                    <?= Html::button(Html::tag("span", null, ["class" => "glyphicon glyphicon-trash", "aria-hidden" => true])." ".Yii::t('client', "Delete"), ["class" => "btn btn-danger controlls hidden", "onclick" => "deleteCategory(this)", "container" => ["update" => "#_view_categories"]]) ?>    
                </div>
            </div>
            <div class="panel-footer">
                <?php Pjax::begin(['id' => '_view_categories']); ?>
                <?= PhotoCategories::buildSlider(10); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
        
        <?= $this->render("_modal", []); ?>
    </div>
    <div classs="col-md-12">
        <h3><?= Yii::t('client', 'Photos'); ?></h3>
        <?php
        echo ptGallery::widget([
            'items' => $items,
            'clientEvents' => [
                'onslide' => 'function(index, slide) {
                    console.log(slide);
                    var text = this.list[index].getAttribute("data-description"),
                    node = this.container.find(".description");
                    node.empty();
                    if (text) {
                        node[0].appendChild(document.createTextNode(text));
                    }
                }'
            ],
        ]);
        ?>
    </div>
</div>
