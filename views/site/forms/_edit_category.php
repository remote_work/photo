<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\models\User;

?>

<div class="categories-form">

    <?php $form = ActiveForm::begin([
        //"action" => "/site/edit-category",
        'options' => ['data-pjax' => 1]
    ]); ?>

    <?= $form->field($model, 'id_user')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', "id" => "_submit_categories_button"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
