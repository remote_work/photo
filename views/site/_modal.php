<?php
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\web\JsExpression;

use app\modules\photo\models\PhotoCategories;

Modal::begin([
    'id' => 'edit-category',
    'header' => '<h3 id="add-category-header"></h3>',
    'clientEvents' => [
        'show.bs.modal' => new JsExpression("function(event){
            var button = $(event.relatedTarget);
            var title = button.data('title');
            $(this).find('#photocategories-title').val(title);
        }"),
    ]
]);


$this->registerJs(
   '$("document").ready(function(){ 
        $("#_edit_category").on("pjax:end", function() {
            $("#edit-category").modal("hide");
            $.pjax.reload({container:"#_view_categories"});
        });
    });'
);


Pjax::begin(['id' => '_edit_category', 'enablePushState' => false]);
echo $this->render("forms/_edit_category", ["model" => new PhotoCategories]);
Pjax::end();
Modal::end();

?>

