<?php
use yii\bootstrap\Html;
use dosamigos\gallery\Gallery;

/* @var $this yii\web\View */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">
    <div classs="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2><?= $model->title ?></h2>
            </div>
            <div class="panel-body">
                <div class="btn-group" role="group">
                    <?= Html::a(Html::tag("span", null, ["class" => "glyphicon glyphicon-photo", "aria-hidden" => true])." ".Yii::t('client', "Upload"),["/site/upload", "id" => $model->id], ["class" => "btn btn-success"]) ?>    
                </div>
            </div>
            <div class="panel-footer">
                <?= Gallery::widget(['items' => $items]); ?>
            </div>
        </div>
    </div>
</div>
