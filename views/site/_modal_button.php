<?php
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\web\JsExpression;

$modal = Modal::begin ([
    'header' => false,
    'toggleButton' => [
        'tag' => 'button',
        'rel-url' => $button['url'],
        //'rel-params' => isset($param)?json_encode($param):false,
        'label' => "<span>{$button['name']}</span>",
        "class" => $button['class'],
    ],

]);

$pjax = Pjax::begin(['enablePushState' => false]);

Pjax::end();

$this->registerJs(
   '$("document").ready(function(){ 
        $("'.$pjax->getId().'").on("pjax:end", function() {
            $("'.$modal->getId().'").modal("hide");
            $.pjax.reload({container:"'.$button['reload'].'"});
        });
    });'
);

Modal::end ();
?>

