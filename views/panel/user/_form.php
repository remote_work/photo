<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php 
    $form = ActiveForm::begin([
        //'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>
    
    <?php 
    echo $form->field($model, 'scenario')->hiddenInput()->label(false);
    echo $form->field($model, 'avatar')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            //'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('users', 'Select Photo'),
            'initialPreview'=> [
                Html::img($model->getThumbUploadUrl('avatar', 'preview'), ['class' => 'img-thumbnail'])
            ], 
            'overwriteInitial'=>true,
        ],
        'pluginEvents' => [
            "fileclear" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('default');}",
            "fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('update')}",
        ],
    ]);
    ?>
    
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php
    if ($model->isNewRecord)
        echo $form->field($model, '_password')->passwordInput(['maxlength' => true]) 
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
