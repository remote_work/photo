<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Yii::t('users', "Users list") ?></h1>

    <p>
        <?= Html::a(Yii::t('users', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'first_name',
            'last_name',
            'email:email',
            // 'password',
            // 'salt',
//            [
//                'attribute' => "role",
//                'format' => 'raw',
//                'value' => function($model) {
//                    $roles = Yii::$app->authManager->getRolesByUser($model->id);
//                    //Yii::$app->debug->show($roles);
//                    $_roles = [];
//                    foreach ($roles as $role) 
//                        $_roles[] = $role->description;
//                    $none = Html::a('<span class="glyphicon glyphicon-plus"></span>', ["permit/user/view", "id" => $model->id]);
//                    return !empty($_roles)?implode(",", $_roles):$none;
//                }
//            ],
            // 'send_delivery_sms',
            // 'id_city',
            // 'balance',
            // 'active',
            // 'activate_key',
            // 'temp_recover_password',
            // 'recover_password_key',
            // 'date_created',
            // 'date_updated',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'P',
                'contentOptions' => ['style' => 'width:30px;'],
                'buttons'=>[
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-education"></span>', ["panel/permit/user/view", "id" => $model->id], [
                            'title' => Yii::t('yii', 'Назначение роли для пользователя'),
                            'data-pjax'=>'0',
                            'target' => '_blank',
                            //'class' => 'grid-action'
                        ]);
                    },
                ],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Pw',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{password}',
                'visible' => Yii::$app->user->can("admin/user/password"),
                'buttons'=>[
                    'password' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', ["admin/user/password", "id" => $model->id], [
                            'title' => Yii::t('users', 'Change password'),
                            'data-pjax'=>'0',
                            //'class' => 'grid-action'
                        ]);
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>
    
    

</div>
