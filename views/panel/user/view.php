<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('users', 'Management roles'), ['permit/user/view', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => Yii::t('users', 'Avatar'),
                'format' => 'raw',
                'value' => Html::img($model->getThumbUploadUrl('avatar', 'preview'), ['class' => 'img-thumbnail', "width" => 100]),
            ],
            'first_name',
            'last_name',
            'email:email',
            //'password',
            //'salt',
//            [
//                'attribute' => 'role',
//                'format' => 'raw',
//                'value' => ($roles = User::getRolesForUser($model->id))?implode(", ", $roles):Html::a('<span class="glyphicon glyphicon-plus"></span>', ["permit/user/view", "id" => $model->id]),
//            ],
            //'send_delivery_sms',
            //'id_city',
            //'activate_key',
            //'temp_recover_password',
            //'recover_password_key',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
