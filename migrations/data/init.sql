--
-- Структура таблицы `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_category` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  `is_delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `photo_categories`
--

CREATE TABLE IF NOT EXISTS `photo_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `is_show` int(11) NOT NULL DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `photo_comments`
--

CREATE TABLE IF NOT EXISTS `photo_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_photo` int(11) DEFAULT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `message` varchar(1024) DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `dislikes` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `translate_languages`
--

CREATE TABLE IF NOT EXISTS `translate_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(3) DEFAULT NULL,
  `icon` text,
  `is_default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `translate_languages`
--

INSERT INTO `translate_languages` (`id`, `name`, `type`, `icon`, `is_default`) VALUES
(1, 'Русский', 'ru', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `translate_messages`
--

CREATE TABLE IF NOT EXISTS `translate_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL,
  `id_language` int(11) DEFAULT NULL,
  `language` varchar(3) DEFAULT NULL,
  `category` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `message` varchar(255) NOT NULL,
  `translation` text NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `avatar`, `status`, `created_at`, `updated_at`) VALUES
(1, 'kitary', 'Федор', 'Вадимович', 'JtKvWGzoqR0n2wOrq2ho60AcfGieNNAm', '$2y$13$bLtB5tLCYL8V3ihDK0Q8FOIECKWQFJpSCyO4D1ud4F8XT8oiO3Ele', '3G8y58FTiI45RjzeH3BQjZeYaM9rKYAL_1466522630', 'mindnighte@gmail.com', NULL, 10, 1466522535, 1466522630);

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('Aministartor', 1, 'Предназначен для администрирования сайта', NULL, NULL, 1467216362, 1467816795),
('panel/permit', 2, 'Работа с доступами', NULL, NULL, 1467815655, 1467815655),
('panel/photo', 2, 'Работа с фотографиями', NULL, NULL, 1467815699, 1467815699),
('panel/translate', 2, 'Работа с переводами', NULL, NULL, 1467816788, 1467816788),
('panel/user', 2, 'Работа с пользователями', NULL, NULL, 1467816764, 1467816764);

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Aministartor', '1', 1467815499);

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Aministartor', 'panel/permit'),
('Aministartor', 'panel/photo'),
('Aministartor', 'panel/translate'),
('Aministartor', 'panel/user');