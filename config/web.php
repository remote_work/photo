<?php
Yii::setAlias('@views', dirname(__DIR__) . '/views');
$params = require(__DIR__ . '/params.php');
$access = require(__DIR__ . '/access.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => "ru",
    'modules' => [
        'gii' => 'yii\gii\Module',
        'photo' => 'app\modules\photo\Photo',
        'translate' => 'app\modules\translate\Translate',
        'permit' => [
            'class' => 'app\modules\permit\Permit',
            'params' => [
                'userClass' => 'app\models\User',
                'rolesFromUser' => true,
            ],
        ],
    ],
    'as AccessBehavior' => [
        'class' => "app\modules\permit\behaviors\AccessBehavior",
        'rules' => $access
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'd0U09R9IvWZnopCPZ1eNGMgA-NrkGGKi',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'debug' => [
            'class' => 'app\components\Debug',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
//                    'class' => 'app\modules\translate\components\DbMessageSource',
//                    'sourceLanguage' => 'en',
//                    'on missingTranslation' => ['app\modules\translate\components\DbMessageSource', 'handleMissingTranslation'],
                    
                ],
            ]
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                'panel'=>'panel/user/index',
                '<language:(ru|en|de)>' => 'site/index',
                
                
                '<controller:\w+>/<action:[\w-]+>'=>'<controller>/<action>',
                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>'=>'<module>/<controller>/<action>',
                
                'panel/<controller:\w+>/<action:[\w-]+>'=>'panel/<controller>/<action>',
                'panel/<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>'=>'<module>/panel/<controller>/<action>',
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
