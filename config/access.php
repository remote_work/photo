<?php
return [
    'admin/panel' => [
        [
            'actions' => ['index', 'error', 'captcha', 'login', 'logout', 'signup'],
            'allow' => true,
        ],
    ],
    'files/default' => [
        [
            'actions' => ['form', 'upload'],
            'allow' => true,
        ],
    ],
    'site' => [
        [
            'actions' => ['index', 'view', 'login', 'signup', 'logout', 'about', 'contact', 'captcha', 'error', 'category', "add-category", "edit-category", "delete-category", "upload"],
            'allow' => true,
        ],
    ],
];

