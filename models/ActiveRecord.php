<?php

namespace app\models;

use Yii;

class ActiveRecord extends \yii\db\ActiveRecord {  
    
    protected static $id_input = 1;
    
    //Для кеширования
    const CACHE_KEY_PREFIX = 'Yii.Models.';
    public $cachingDuration = 30;
    public $cacheID = 'cache';
    
    protected function cashed($key = false) {
        $component = $this->cacheID;
        if ($this->cachingDuration > 0 && $component !== false && ($cache = Yii::$app->$component) !== null && $key) {
            if (($data = $cache->get($key)) !== false) {
                return unserialize($data);
            }
        }
        return false;
    }
    
    public static function getShortName($lower = false) {
        $name = (new \ReflectionClass(self::className()))->getShortName();
        return  ($lower)?strtolower($name):$name; 
    }
        
    public function prepare($data) {
        $class = $this::getShortName();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $this->attributes)) {
                $data[$class][$key] = $value;
            }
        }
        return $this->load($data);
    }
    
    public function loadScenario($default = false, $id_item = false) {
        $class = $this::getShortName();
        $post = Yii::$app->request->post();
        if (!$id_item)
            $this->scenario = (isset($post[$class]['scenario']))?$post[$class]['scenario']:($default?$default:"default");
        else 
            $this->scenario = (isset($post[$class][$id_item]['scenario']))?$post[$class][$id_item]['scenario']:($default?$default:"default");
    }
    
    public function fieldsList($language = "ru") {
        $fields = [];
        foreach ($this->attributes as $key => $value) {
            $matche = null;
            if (preg_match('/\\w+_(ru|en)$/', $key, $matche)) {
                if ($matche[1] == $language)
                    $fields[] = $key;
                else continue;
            }
            else $fields[] = $key;
                
            
        }
        return $fields;
    }
    
    public static function getIdForInput() {
        return "wd".self::$id_input++;
    }
    
    //Модифицирует массив $_FILES согласно текущей м
    public function sortFilesVariable($files = [], $id = false, $reset = true) {
        $class = $this->getShortName();
        $_files = [];
        if ($reset)
            \yii\web\UploadedFile::reset();
        if (isset($files[$class])) {
            foreach ($files[$class] as $key => $var) {
                if (isset($var[$id]))
                    $_files[$class][$key] = $files[$class][$key][$id];
            }
        }
        $_FILES = $_files;
    }
    
    public static function loadModels($models = false, $post = false) {
        if (!$post)
            $post = Yii::$app->request->post();
        if ($models) {
            $result = false;
            foreach ($models as &$model) {
                if (isset($post[$model->getShortName()][$model->id])) {
                    $result = true;
                    $model->prepare($post[$model->getShortName()][$model->id]);
                }
            }
            if ($result)
                return $models;
        }
        return false;
    }
    
    public function isDefaultScenario() {
        return ($this->scenario == "default")?true:false;
    }
   
}

