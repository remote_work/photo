<?php

namespace app\models;

use app\models\User;
use app\models\Mail;
use Yii;

/**
 * Signup form
 */
class SignupForm extends \yii\base\Model {

    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $email;
    public $status;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            [['username', 'first_name', 'email'], 'required'],
            ['email', 'email'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['first_name', 'last_name'], 'string'],
            ['status', 'default', 'value' => User::STATUS_NEW],
        ];
    }
    
//    public function scenarios() {
//        $scenarios = parent::scenarios();
//        $scenarios['signup'] = ['username', 'first_name', 'password'];
//        return $scenarios;
//    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($scenario = "signup") {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->setPassword($this->password);
            $user->created_at = date("Y-m-d G:i:s", time());
            $user->status = User::STATUS_NEW;
            if ($user->validate()) {
                if ($user->save()) {
                    if ($userRole = Yii::$app->authManager->getRole('user'))
                        Yii::$app->authManager->assign($userRole, $user->getId());

                    return $user;
                }
            }
            else $this->addErrors ($user->getErrors ());
        }
    }

}
