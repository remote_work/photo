<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

use app\models\User;

class UserSearch extends User {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'username', 'first_name', 'last_name', 'email', 'avatar'], 'safe'],
        ];
    }
    
//    /**
//    * @inheritdoc
//    */
//    public function scenarios() {
//        // bypass scenarios() implementation in the parent class
//        return Model::scenarios();
//    }

    public function search($params, $id = false) {
        $query = ($id) ? UserSearch::find()->where(["id_user" => $id]) : UserSearch::find();
        $query->select("user.*");
        $query->joinWith([
//            'section' => function($query) {
//                $query->from(['section' => 'section']);
//            },
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];

//        $dataProvider->sort->attributes['username'] = [
//            'asc' => ['user.email' => SORT_ASC],
//            'desc' => ['user.email' => SORT_DESC],
//        ];

        //print_r($query->createCommand()->getRawSql()); die();
        

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'first_name', $this->first_name]);
        $query->andFilterWhere(['like', 'last_name', $this->last_name]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

}
        